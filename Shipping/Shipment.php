<?php namespace MerchPlatform\Shipping;

class Shipment {

    protected $packageTypes;
    protected $packages = array();
    protected $shipmentItems;
    protected $parentEntity = array();

    // each shipmentItems item should have a
    //  - any unique "id" attribute for the type of item (because it can be grouped more than one inside of the package), qty, a fits_poly, fits_box_s, fits_box_m, fits_box_l, and weight in ounces (per unit)
    //  In the case of packing items for campaign orders, the "id" would be the campaign_item.id
    //  In the case of a bulk shipment, it would be the line_variant.id
    public function __construct($shipmentItems, $entityOptions)
    {
        // Get and set package types from DB
        $this->packageTypes = \MerchPlatform\Entities\PackageType::select('id', 'name', 'short_name', 'weight_oz', 'length', 'width', 'height')->get();

        // Get and set the shipment's items, initializing them as well (total qty actually packed into a package set to 0)
        $this->shipmentItems = $shipmentItems;
        foreach($this->shipmentItems as $item) {
            $item['total_packed'] = 0;
        }
        
        // This is solely for mapping to a campaign_order or order now order
        $this->parentEntity = $entityOptions;
    }

    // Get a collection of all the shipment's packages
    public function getPackages()
    {
        return \Illuminate\Database\Eloquent\Collection::make($this->packages);
    }

    // Pack all shipment items into only poly mailers
    public function packPolyMailers()
    {
        while(!$this->isDonePacking()) {
            $this->doPacking('poly');
        }
    }

    // Check if there are still any items that haven't yet been placed into a package (if there are unpacked items, this returns false, else true)
    public function isDonePacking()
    {
        $donePacking = true;
        foreach($this->shipmentItems as $idx => $item)
        {
            if($this->shipmentItems[$idx]['total_packed'] < $this->shipmentItems[$idx]['qty']) {
                $donePacking = false;
            }
        }

        return $donePacking;
    }

    // Do the packing of items into package object(s)
    public function doPacking($containerTypeName)
    {
        foreach($this->shipmentItems as $idx => $item)
        {
            if($this->shipmentItems[$idx]['total_packed'] < $item['qty']) {
                $wasPacked = false;
                foreach($this->packages as $package)
                {
                    if($package->canFitItem($item)) {
                        $package->addItem($item);
                        $this->shipmentItems[$idx]['total_packed']++;
                        $wasPacked = true;
                        break;
                    }
                }

                if(!$wasPacked) {
                    $packageType = $this->getPackageTypeByName($containerTypeName);
                    array_push($this->packages, new \MerchPlatform\Shipping\Package($packageType, $this->shipmentItems));
                }
            }
        }
    }

    // This will actually add the packages to the database, and map them to an order or campaign_order - don't know if this is necessary
    // public function addToDB()
    // {
    //     if(empty($this->parentEntity)) {
    //         return false;
    //     }
        
    //     $this->getPackages()->each(function($package){
    //         $package->{$this->parentEntity['column']} = $this->parentEntity['id'];
    //         $package->save();
            
    //         $package->packageItems->each(function($item) use($package) {
    //             $item->{$item->unique_id_column} = $item->unique_id;
    //             unset($item->unique_id_column);
    //             unset($item->unique_id);
    //             unset($item->fits_poly);
    //             $package->packageItems()->save($item);
    //         });
    //     });
    //     echo('added');
    //     return true;
    // }

    // Shortcut function to get package type object by its name
    private function getPackageTypeByName($name, $shortOrLong = 'short')
    {
        return $this->packageTypes->filter(function($type) use($name, $shortOrLong) {
            if($shortOrLong == 'long') {
                if($type->name == $name) return true;
            } else {
                if($type->short_name == $name) return true;
            }
            
        })->first();
    }

}