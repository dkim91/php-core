<?php namespace MerchPlatform\Shipping;

class Package extends \MerchPlatform\Entities\Package {

    public $container;
    protected $totalSlots;
    
    public function __construct($packageType, $orderItems)
    {
        $this->weight_oz = $packageType->weight_oz;
        $this->container = $packageType->toArray();
        $this->packageType = $packageType;
        $this->initializeSlots($orderItems);
    }

    // Set the total number of "slots" that this package has available for items
    protected function initializeSlots($orderItems)
    {
        $qtyPackageFitsByItem = array();
        foreach($orderItems as $item) {
            array_push($qtyPackageFitsByItem, $item['fits_'.$this->packageType->short_name]);
        }

        $this->totalSlots = $this->get_lcm(array_unique($qtyPackageFitsByItem));
    }

    // Get the number of "slots" open/free/unused that this package has left for items
    public function getEmptySlots()
    {
        $slotsFilled = 0;
        foreach($this->packageItems as $item) {
            $slotsFilled += $this->numSlotsRequired($item)*$item->qty;
        }

        return $this->totalSlots-$slotsFilled;
    }

    // Get the number of total "slots" this package has for items
    public function getTotalSlots()
    {
        return $this->totalSlots;
    }

    // Insert a single item into the package
    public function addItem($itemToPack)
    {  
        $packed = false;
        foreach($this->packageItems as $item) {
            if($item->unique_id == $itemToPack['unique_id']) {
                $packed = true;
                $item->qty++;
            }
        }
        if(!$packed) {
            $item = new \MerchPlatform\Entities\PackageItem;
            $item->unique_id_column = $itemToPack['unique_id_column'];
            $item->unique_id = $itemToPack['unique_id'];
            $item->qty = 1;
            $item->{'fits_'.$this->packageType->short_name} = $itemToPack['fits_'.$this->packageType->short_name];

            $this->packageItems->add($item);
        }

        $this->weight_oz += $itemToPack['weight'];

        // echo('added item to package. total slots: '.$this->getTotalSlots(). ', empty slots: '.$this->getEmptySlots(). '<br>');
    }

    // Check if this package has enough empty slots to fit a given item
    public function canFitItem($item)
    {
        if($this->getEmptySlots() >= $this->numSlotsRequired($item))
        {
            return true;
        }

        return false;
    }

    // Get all items in the package
    public function getItems()
    {
        return $this->packageItems;
    }

    // Check how many slots of this package a given item would require / fill
    private function numSlotsRequired($item)
    {
        return $this->totalSlots/$item['fits_'.$this->packageType->short_name];
    }

    
    /* Math stuff below this point */
    // Get least common multiple
    private function get_lcm($items)
    {
        while(2 <= count($items))
        {
            array_push($items, $this->lcm(array_shift($items), array_shift($items)));
        }
        
        return reset($items);
    }

    // Calculate least common multiple
    private function lcm($n, $m)
    {
       return $m*($n/$this->gcd($n,$m));
    }

    // Calculate greatest common denominator
    private function gcd($n, $m)
    {
        $n=abs($n); $m=abs($m);
        if ($n==0 and $m==0)
            return 1; //avoid infinite recursion
        if($n==$m and $n>=1)
            return $n;
        
        return $m<$n?$this->gcd($n-$m,$n):$this->gcd($n,$m-$n);
    }
}