<?php namespace MerchPlatform\Finance;

use MerchPlatform\Finance\BaseQuote;
use MerchPlatform\Entities\Campaign;
use MerchPlatform\Entities\SupplierOrderItem;
use MerchPlatform\Entities\Job;
use MerchPlatform\Entities\ScrRange;
use MerchPlatform\Entities\Design;
use Illuminate\Database\Capsule\Manager as DB;

class CampaignQuote extends BaseQuote
{
    public function __construct()
    {
        parent::__construct();
    }

    public function setCampaign($campaignId)
    {
        $this->campaign = Campaign::with(array(
            'campaignOrders' => function($q) {
                $q->whereStatus('paid');
            }))->find($campaignId);
        $this->campaignOrders = $this->campaign->campaignOrders;
        $this->numOrders = $this->campaignOrders->count();
    }

    public function getProcessingCosts()
    {
        $costs = array();

        $percentage = $this->costSettings->cost_per_transaction_pct;
        $flat = $this->costSettings->cost_per_transaction_flat;
        $orderTotals = $this->campaignOrders->lists('price_total');
        $income = array_sum($orderTotals);
        $costs[] = $this->getPaymentsCost(
            $income,
            $this->numOrders,
            $percentage,
            $flat
        );

        return $this->getCostsArray($costs);
    }

    public function getPaymentsCost($income, $numOrders, $percentage, $flat)
    {
        $total = ($numOrders * $flat);
        $total += ($income * ($percentage / 100));
        $costPer = ($total / $numOrders);
        $costArray = $this->getCostArray(
            'payments',
            'process',
            $numOrders,
            $costPer
        );

        return $costArray;
    }
}
