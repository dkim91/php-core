<?php
namespace MerchPlatform\Messaging;

use Predis\Client;
use cgrafton\Kue\KueApi;
/**
 * Sends jobs to Kue client Queues
 */
class Queue
{
    /**
     * Instantiate Kue Client.
     */
    public function __construct()
    {
        $this->kueRedis = new Client([
          'host' => \MerchPlatform\Config\Main::get('redis')['host'],
          'port' => \MerchPlatform\Config\Main::get('redis')['port'],
          'database' => \MerchPlatform\Config\Main::get('redis')['db']
        ]);
        $this->kue = new KueApi($this->kueRedis);
    }

    /**
     * Add a message to an Kue queue. This method will issue 2 http requests
     * @param  mixed $message Message to add. Will be json-encoded.
     * @param  $queueName   string Name of the kue queue
     */
    public function add($message, $queueName)
    {

        $this->kue->createJob($queueName, $message);

    }

    /*
     * Add a message to a Kue queue by queue name.
     * @param  mixed $message Message to add. Will be json-encoded.
     * @param  $queueName name of the queue that we are pushing message onto
    */
    public function addToQueueURL($message, $queueName)
    {
        $this->kue->createJob($queueName, array(
                'MessageBody' => json_encode($message)
            )
        );
    }

    /*
        * Add a bunch of messages to a Kue queue by queue name.
        * @param  $messages array Messages to add. Each item will be json-encoded.
        * DEPRECATED: Keep in mind SQS API Limitations(256Kb, encoding)
        * DEPRECATED: http://docs.aws.amazon.com/aws-sdk-php/v2/api/class-Aws.Sqs.SqsClient.html#_sendMessageBatch
        * @param  $queueName name of the queue that we are pushing message onto
       */
    public function batchAddToQueue($messages, $queueName)
    {
        $this->kue->createJob($queueName, array(
                'entries' => $messages
            )
        );
    }
}
