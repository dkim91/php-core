<?php namespace MerchPlatform\Files;

use finfo;
use MerchPlatform\Files\Exceptions\MimeTypeValidatorNotFound;

class Validator
{
    private $allowed = array();

    private $maxSize;

    private $file;

    private $error;

    public function __construct($file, $allowed, $maxSize = null)
    {
        $this->file = $file;
        $this->allowed = $this->parseAllowed($allowed);
        $this->maxSize = $maxSize;
    }

    private function parseAllowed($allowed)
    {
        return is_array($allowed) ? $allowed : explode('|', $allowed);
    }

    public function setError($error = null)
    {
        $this->error = $error;
        return $this;
    }

    public function getError()
    {
        return $this->error;
    }

    public function validates()
    {
        if (is_null($this->file))
        {
            $this->setError('no_file_sent');
            return false;
        }
        switch ($this->file['error']) {
            case UPLOAD_ERR_OK:
            break;
            case UPLOAD_ERR_NO_FILE:
                $this->setError('no_file_sent');
                return false;
            break;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $this->setError('file_size_exceeded');
                return false;
            break;
            default:
                $this->setError('unknown_error');
                return false;
            break;
        }
        if (!is_null($this->maxSize) && $this->file['size'] > $this->maxSize) {
            $this->setError('file_size_exceeded');
            return false;
        }
        $path = pathinfo($this->file['name']);
        $fileInfo = new finfo(FILEINFO_MIME_TYPE);
        $actualMimeTypes = $this->getMimeTypes($path['extension']);
        if (!in_array($fileInfo->file($this->file['tmp_name']), $actualMimeTypes)) {
            if (!$this->tryToCorrectInvalidMime($this->file)){
                $this->setError('invalid_file_type');
                return false;
            }
        }
        return true;
    }

    private function tryToCorrectInvalidMime(&$thisFile)
    {
        switch(exif_imagetype($thisFile['tmp_name'])) {
            case IMAGETYPE_GIF:
                $newExt = "gif";
                $newMime = "image/gif";
                break;
            case IMAGETYPE_JPEG:
                $newExt = "jpg";
                $newMime = "image/jpeg";
                break;
            case IMAGETYPE_PNG:
                $newExt = "png";
                $newMime = "image/png";
                break;
            case IMAGETYPE_SWF:
                $newExt = "swf";
                $newMime = "application/x-shockwave-flash";
                break;
            case IMAGETYPE_PSD:
                $newExt = "psd";
                $newMime = "image/vnd.adobe.photoshop";
                break;
            case IMAGETYPE_BMP:
                $newExt = "bmp";
                $newMime = "image/bmp";
                break;
            case IMAGETYPE_TIFF_II:
            case IMAGETYPE_TIFF_MM:
                $newExt = "tif";
                $newMime = "image/tiff";
                break;
            default:
                return false;
        }
        $thisFile['name'] = preg_replace('/\.(\w+)$/', ".$newExt", $this->file['name']);
        $thisFile['type'] = $newMime;
        return true;
    }

    private function getMimeTypes($extension = null)
    {
        if (is_null($extension)) return array();
        $mimeType = $this->buildMimeTypeValidator($extension);
        return $mimeType->getMimeTypes();
    }

    private function buildMimeTypeValidator($extension)
    {
        $name = ucwords($extension);
        $class = 'MerchPlatform\\Files\\Validator\\MimeTypes\\'.$name;
        if (!class_exists($class)) throw new MimeTypeValidatorNotFound("MimeType validator for ({$name}) not found.");
        return new $class;
    }

    public function fails()
    {
        return !$this->validates();
    }

    public function passes()
    {
        return $this->validates();
    }

    public function isValid()
    {
        return $this->passes();
    }

    public function isInvalid()
    {
        return $this->fails();
    }

    public function valid()
    {
        return $this->isValid();
    }

    public function invalid()
    {
        return $this->isInvalid();
    }


}