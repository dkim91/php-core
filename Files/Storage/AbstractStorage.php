<?php namespace MerchPlatform\Files\Storage;

abstract class AbstractStorage
{
    protected $options = array();

    public function setOptions(array $options = array())
    {
        $this->options = $options;
    }

    public function getOption($key = null)
    {
        return isset($this->options[$key]) ? $this->options[$key] : null;
    }

}