<?php namespace MerchPlatform\Files\Storage;

interface StorageInterface {

    public function setOptions(array $options = array());
    public function getOption($key = null);
    public function storage();

}