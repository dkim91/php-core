<?php namespace MerchPlatform\Files;

use LogicException;
use League\Flysystem\Filesystem;

class File {

    const VISIBILITY_UNSUPPORTED_DEFAULT = 'public';

    private $filesystem;
    private $filename;

    public function __construct(Filesystem $filesystem, $filename)
    {
        $this->filesystem = $filesystem;
        $this->filename = $filename;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function getContents()
    {
        return $this->filesystem->read($this->getFilename());
    }

    public function getMimetype()
    {
        return $this->filesystem->getMimetype($this->getFilename());
    }

    public function getTimestamp()
    {
        return $this->filesystem->getTimestamp($this->getFilename());
    }

    public function getSize()
    {
        return $this->filesystem->getSize($this->getFilename());
    }

    public function getVisibility()
    {
        try {
            return $this->filesystem->getVisibility($this->getFilename());
        } catch(LogicException $exception) {
            return self::VISIBILITY_UNSUPPORTED_DEFAULT;
        }
    }

    public function setContents($contents)
    {
        return $this->filesystem->write($this->getFilename(), $contents);
    }

    public function delete()
    {
        return $this->filesystem->delete($this->getFilename());
    }

    public function rename($name)
    {
        return $this->filesystem->rename($this->getFilename(), $name);
    }
}