<?php namespace MerchPlatform\Files\Validator;

interface MimeTypeInterface
{
    public function getExtension();
    public function getMimeTypes();
}