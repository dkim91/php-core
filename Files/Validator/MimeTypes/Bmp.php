<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Bmp implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'bmp';
    }

    public function getMimeTypes()
    {
        return array('image/bmp', 'image/x-windows-bmp', 'image/x-ms-bmp');
    }
}