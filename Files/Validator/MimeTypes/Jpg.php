<?php namespace MerchPlatform\Files\Validator\MimeTypes;

use MerchPlatform\Files\Validator\MimeTypeInterface;

class Jpg implements MimeTypeInterface
{
    public function getExtension()
    {
        return 'jpg';
    }

    public function getMimeTypes()
    {
        return array('image/jpeg', 'image/pjpeg');
    }
}