<?php namespace MerchPlatform\Files;

use RuntimeException;

class Details
{
    protected $filesystem;
    protected $item;

    public function __construct($filesystem, $item)
    {
        $this->filesystem = $filesystem;
        $this->item = $item;
    }

    public function __call($method, $arguments)
    {
        if (array_key_exists($method, $this->item)) {
            return $this->item[$method];
        }
        throw new RuntimeException("{$method} does not exist.");
    }

    public function name()
    {
        return $this->filename();
    }

    public function type()
    {
        return $this->extension();
    }

    public function contents()
    {
        return $this->filesystem->read($this->path());
    }

    public function delete()
    {
        return $this->filesystem->delete($this->path());
    }

    public function rename($newName = null)
    {
        if (!is_null($newName)) {
            return $this->filesystem->rename($this->path(), $newName);
        }
        throw new RuntimeException("No new name supplied to rename {$this->path()}");
    }

    public function duplicate($newName = null)
    {
        $newName = $newName ?: str_replace($this->filename(), $this->filename().'_'.time(), $this->path());
        return $this->filesystem->write($newName, $this->contents());
    }
}