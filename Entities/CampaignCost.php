<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignCost extends Eloquent
{
    protected $fillable = array(
        'campaign_id', 'name', 'type', 'units', 'cost',
        'accuracy', 'job_id', 'meta'
    );

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function job()
    {
        return $this->belongsTo('MerchPlatform\Entities\Job');
    }

    public function getCostAttribute($value)
    {
        return $value / 100;
    }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = $value * 100;
    }
}