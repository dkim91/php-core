<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignTag extends Eloquent
{
    protected $fillable = array(
        'campaign_id',
        'tag_id'
    );
    protected $appends = array();
    protected $hidden = array();

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function tag()
    {
        return $this->belongsTo('MerchPlatform\Entities\Tag');
    }
}
