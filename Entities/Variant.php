<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Variant extends Eloquent
{
    protected $fillable = [
        'product_id',
        'size_id',
        'color_id',
        'gtin',
        'weight',
        'attr_length',
        'attr_width',
        'is_active',
        'min_cost',
        'max_cost',
        'extra_cost',
        'dtg_scalar',
    ];
    protected $appends = array();
    protected $hidden = array();

    public function childVariants()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Variant', 'variant_relationships', 'parent_id', 'child_id')->withPivot('priority')->orderBy('variant_relationships.priority');
    }

    public function color()
    {
        return $this->belongsTo('MerchPlatform\Entities\Color');
    }

    public function product()
    {
        return $this->belongsTo('MerchPlatform\Entities\Product');
    }

    public function size()
    {
        return $this->belongsTo('MerchPlatform\Entities\Size');
    }

    public function lineVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\LineVariant');
    }

    public function priceListItems()
    {
        return $this->hasMany('MerchPlatform\Entities\PriceListItem');
    }

    public function skus()
    {
        return $this->hasMany('MerchPlatform\Entities\Sku');
    }

    public function getWeightOzAttribute()
    {
        return round($this->attributes['weight']*16, 2);
    }

    public function getMinCostAttribute($value)
    {
        return $value / 100;
    }

    public function setMinCostAttribute($value)
    {
        $this->attributes['min_cost'] = $value * 100;
    }

    public function getMaxCostAttribute($value)
    {
        return $value / 100;
    }

    public function setMaxCostAttribute($value)
    {
        $this->attributes['max_cost'] = $value * 100;
    }

    public function getExtraCostAttribute($value)
    {
        return $value / 100;
    }

    public function setExtraCostAttribute($value)
    {
        $this->attributes['extra_cost'] = $value * 100;
    }

    public function scopeGetFromGroupsQuoteRequest($query, $groups)
    {
        foreach($groups as $group)
        {
            foreach($group['products'] as $product)
            {
                $productIds[] = $product['product_id'];
                $colorIds[] = $product['color_id'];
            }
        }

        $productIds = array_unique($productIds);
        $colorIds = array_unique($colorIds);

        return $query->whereIn('product_id', $productIds)->whereIn('color_id', $colorIds);
    }

    public static function gtinQtysToAdvancedFormat($gtinQtys)
    {
        $gtins = array();
        $total = array('not_arrived' => 0, 'arrived' => 0, 'issue' => 0, 'all' => 0);
        foreach ($gtinQtys as $gtin => $qty) {
            $gtins[] = $gtin;
        }

        $variants = \MerchPlatform\Entities\Variant::with('product', 'color', 'size')
            ->whereIn('gtin', $gtins)
            ->get();
        $sizes = \MerchPlatform\Entities\Size::where('name', '!=', 'OSFA')->get();

        // Create initialSizes
        foreach ($sizes as $size) {
            $initialSizes[$size->name] = array('not_arrived' => 0, 'arrived' => 0, 'issue' => 0);
        }

        $styleColors = array();
        foreach ($variants as $variant) {
            $styleKey = $variant->product->code;
            $colorKey = $variant->color->name;

            if (!isset($styleColors[$styleKey][$colorKey]['sizes'])) {
                $styleColors[$styleKey][$colorKey]['sizes'] = array(
                    "numberBySizes" => $initialSizes,
                    //"gtin" => $variant->gtin,
                    "total" => array('not_arrived' => 0, 'arrived' => 0, 'issue' => 0)
                );
            }

            foreach ($gtinQtys as $gtin => $gtinQtyArray) {
                if ($variant->gtin === $gtin) {
                    $styleColors[$styleKey][$colorKey]['sizes']['numberBySizes'][$variant->size->name] = $gtinQtyArray;
                    $styleColors[$styleKey][$colorKey]['sizes']['numberBySizes'][$variant->size->name]['gtin']=$gtin;

                    $styleColors[$styleKey][$colorKey]['sizes']['total']['not_arrived'] += $gtinQtyArray['not_arrived'];
                    $styleColors[$styleKey][$colorKey]['sizes']['total']['arrived'] += $gtinQtyArray['arrived'];
                    $styleColors[$styleKey][$colorKey]['sizes']['total']['issue'] += $gtinQtyArray['issue'];

                    $total['not_arrived']+=$gtinQtyArray['not_arrived'];
                    $total['arrived']+=$gtinQtyArray['arrived'];
                    $total['issue']+=$gtinQtyArray['issue'];
                    $total['all']+=$gtinQtyArray['not_arrived'] + $gtinQtyArray['arrived'] + $gtinQtyArray['issue'];
                    //$total += $gtinQtyArray['not_arrived'] + $gtinQtyArray['arrived'] + $gtinQtyArray['issue'];
                }

            }
        }

        return array('styleColors' => $styleColors, 'total' => $total);
    }

    public static function gtinQtysToNiceFormat($gtinQtys)
    {
        $gtins = array();
        $total = 0;
        foreach ($gtinQtys as $gtin => $qty) {
            $gtins[] = $gtin;
        }

        $variants = \MerchPlatform\Entities\Variant::with('product', 'color', 'size')
            ->whereIn('gtin', $gtins)
            ->get();
        $sizes = \MerchPlatform\Entities\Size::where('name', '!=', 'OSFA')->get();

        // Create initialSizes
        foreach ($sizes as $size) {
            $initialSizes[$size->name] = 0;
        }
        $initialSizes['total'] = 0;
        $styleColors = array();
        foreach ($variants as $variant) {
            $styleKey = $variant->product->code;
            $colorKey = $variant->color->name;

            if (!isset($styleColors[$styleKey][$colorKey]['sizes'])) {
                $styleColors[$styleKey][$colorKey]['sizes'] = $initialSizes;
            }

            foreach ($gtinQtys as $gtin => $gtinQty) {
                if ($variant->gtin === $gtin) {
                    $styleColors[$styleKey][$colorKey]['sizes'][$variant->size->name] += $gtinQty;
                    $styleColors[$styleKey][$colorKey]['sizes']['total'] += $gtinQty;
                    $total += $gtinQty;
                }

            }
        }

        return array('styleColors' => $styleColors, 'total' => $total);
    }

    public static function getByProductColorSizeArray(array $queryOpts)
    {
        $variants = self::with(array('color', 'product', 'size'));
        $firstItem = true;

        foreach ($queryOpts as $queryOpt) {
            $whereFunction = function ($query) use ($queryOpt) {
                $query->where('color_id', $queryOpt['color_id'])
                    ->where('size_id', $queryOpt['size_id'])
                    ->where('product_id', $queryOpt['product_id']);
            };

            if ($firstItem) {
                $variants->where($whereFunction);
                $firstItem = false;
            } else {
                $variants->orWhere($whereFunction);
            }
        }
        return $variants->get();

    }


}
