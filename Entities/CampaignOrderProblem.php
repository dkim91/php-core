<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Capsule\Manager as DBManager;
use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignOrderProblem extends Eloquent
{
    protected $fillable = array('order_id', 'problem_id', 'description', 'images');
    protected $appends = array();
    protected $hidden = array();

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order', 'order_id');
    }

    public function problem()
    {
        return $this->belongsTo('MerchPlatform\Entities\Problem', 'problem_id');
    }
}