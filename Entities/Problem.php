<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Problem extends Eloquent
{
    protected $fillable = array('name', 'status');
    protected $appends = array();
    protected $hidden = array();

}