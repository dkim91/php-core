<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Address extends Eloquent
{
    protected $fillable = [
        'customer_id',
        'country_id',
        'state_id',
        'name',
        'company',
        'street_1',
        'street_2',
        'locality',
        'province',
        'postal_code',
        'phone_num',
    ];
    protected $appends = array();
    protected $hidden = array();

    public function country()
    {
        return $this->belongsTo('MerchPlatform\Entities\Country');
    }

    public function state()
    {
        return $this->belongsTo('MerchPlatform\Entities\State');
    }

    public function admin()
    {
        return $this->belongsTo('MerchPlatform\Entities\Admin');
    }

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function supplier()
    {
        return $this->belongsTo('MerchPlatform\Entities\Supplier');
    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }

    public function getIsInternationalAttribute()
    {
        if($this->country_id == '233') {
            return false;
        }
        
        return true;
    }

}