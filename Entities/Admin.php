<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Admin extends Eloquent
{
    protected $fillable = [
        'role_id',
        'login_at',
        'ip_lastlogin',
        'ip_signup',
        'is_disabled',
        'stripe_rec_token',
        'paypal_email',
        'first_name',
        'last_name',
        'email',
        'title',
        'is_superadmin',
        'timezone',
    ];
    protected $appends = array('full_name');
    protected $hidden = array('pass');

    public function role()
    {
        return $this->belongsTo('MerchPlatform\Entities\Role');
    }

    public function address()
    {
        return $this->hasOne('MerchPlatform\Entities\Address');
    }

    public function cards()
    {
        return $this->hasMany('MerchPlatform\Entities\Card');
    }

    public function cart()
    {
        return $this->hasOne('MerchPlatform\Entities\Cart');
    }

    public function contractors()
    {
        return $this->hasMany('MerchPlatform\Entities\Contractor');
    }

    public function contractorUsers()
    {
        return $this->hasMany('MerchPlatform\Entities\ContractorUser');
    }

    public function coupons()
    {
        return $this->hasMany('MerchPlatform\Entities\Coupon');
    }

    public function customers()
    {
        return $this->hasMany('MerchPlatform\Entities\Customer');
    }

    public function groups()
    {
        return $this->hasMany('MerchPlatform\Entities\Group');
    }

    public function campaigns()
    {
        return $this->hasMany('MerchPlatform\Entities\Campaign');
    }

    public function orders()
    {
        return $this->hasMany('MerchPlatform\Entities\Order');
    }

    public function payouts()
    {
        return $this->hasMany('MerchPlatform\Entities\Payout');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

}