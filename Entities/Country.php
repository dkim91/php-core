<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Country extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();

    public function addresses()
    {
        return $this->hasMany('MerchPlatform\Entities\Address');
    }

    public function getShippingAddRateAttribute($value)
    {
        return $value / 100;
    }

    public function setShippingAddRateAttribute($value)
    {
        $this->attributes['shipping_add_rate'] = $value * 100;
    }

    public function getShippingFlatRateAttribute($value)
    {
        return $value / 100;
    }

    public function setShippingFlatRateAttribute($value)
    {
        $this->attributes['shipping_flat_rate'] = $value * 100;
    }
}

