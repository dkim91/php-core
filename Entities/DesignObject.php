<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class DesignObject extends Eloquent
{
    protected $fillable = array('json_data', 'file_id', 'uid', 'width_in', 'height_in', 'top_in', 'left_in', 'angle', 'right_in', 'bottom_in');
    protected $appends = array();
    protected $hidden = array();
    
    public function group()
    {
        return $this->belongsTo('MerchPlatform\Entities\Group');
    }

    public function design()
    {
        return $this->belongsTo('MerchPlatform\Entities\Design');
    }

    public function decColors()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DecColor', 'dec_color_design', 'design_object_id', 'dec_color_id');
    }

    public function file()
    {
        return $this->belongsTo('MerchPlatform\Entities\File');
    }

}
