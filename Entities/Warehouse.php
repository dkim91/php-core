<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Warehouse extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();
    
    public function supplier()
    {
        return $this->belongsTo('MerchPlatform\Entities\Supplier');
    }

    public function stocks()
    {
        return $this->hasMany('MerchPlatform\Entities\Stock');
    }

    public function supplierOrders()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrder');
    }

}