<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmailCampaign extends Eloquent
{
    protected $fillable = array('marketing_email_id', 'campaign_id');

    public function marketingEmail()
    {
        return $this->belongsTo('MerchPlatform\Entities\MarketingEmail');

    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');

    }
}