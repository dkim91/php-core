<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ContractorUser extends Eloquent
{
    protected $fillable = [
        'contractor_id',
        'login_at',
        'is_primary',
        'is_disabled',
        'first_name',
        'last_name',
        'email',
        'ip_lastlogin',
        'ip_signup',
        'role',
        'timezone',
    ];
    protected $appends = array('full_name');
    protected $hidden = array('pass');

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
}