<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PaymentSource extends Eloquent
{
    protected $fillable = array(
        'customer_id',
        'metadata',
        'source_type',
        'source_service'
    );

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function payments()
    {
        return $this->hasMany('MerchPlatform\Entities\Payment');
    }
}