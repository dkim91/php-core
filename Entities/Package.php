<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Package extends Eloquent
{
    protected $fillable = array(
        'order_id',
        'campaign_order_id',
        'package_type_id',
        'address_id',
        'cost',
        'weight_oz',
        'label_url',
        'tracking_num',
        'thirdparty_id',
        'label_line_1',
        'label_line_2',
        'label_line_3',
        'notes',
        'status',
        'tracking_status',
        'tracking_message',
        'tracking_location',
        'estimated_delivery',
        'status_at',
        'length',
        'width',
        'height',
        'package_batch_id',
        'carrier');
    protected $appends = array();
    protected $hidden = array();

    public function getCostAttribute($value)
    {
        return $value / 100;
    }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = $value * 100;
    }

    public function shippingEvents()
    {
        return $this->hasMany('MerchPlatform\Entities\ShippingEvent');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function orderReturn()
    {
        return $this->belongsTo('MerchPlatform\Entities\OrderReturn');
    }

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }

    public function packageType()
    {
        return $this->belongsTo('MerchPlatform\Entities\PackageType');
    }

    public function packageBatch()
    {
        return $this->belongsTo('MerchPlatform\Entities\PackageBatch');
    }

    public function address()
    {
        return $this->belongsTo('MerchPlatform\Entities\Address');
    }

    public function packageItems()
    {
        return $this->hasMany('MerchPlatform\Entities\PackageItem');
    }

    public function supplierOrderItems()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrderItem');
    }

    public function getItemsStringAttribute()
    {
        $items = array();
        foreach ($this->supplierOrderItems as $item) {
            if (!isset($items[$item->variant_id])) {
                $items[$item->variant_id] = array(
                    'qty' => 0,
                    'variant' => $item->variant
                );
            }
            $items[$item->variant_id]['qty']++;
        }

        $this->theString = '';
        foreach ($items as $item) {
            $this->theString .=  $item['qty'].' x '.$item['variant']->size->name .
                ' ' . $item['variant']->color->name . ' ' .
                $item['variant']->product->name . '<br><br>';
        }

        return $this->theString;
    }

    public function getAddressStringAttribute()
    {
        $addressString = '';
        $addressString .= $this->address->first_name.' '.$this->address->last_name.'<br>';
        $addressString .= $this->address->street_1.'<br>';
        $addressString .= ($this->address->street_2 ? $this->address->street_2.'<br>': '');
        $addressString .= $this->address->locality.', '.$this->address->province.' '.$this->address->postal_code.'<br>';
        $addressString .= $this->address->country->name.'<br>';
        $addressString .= $this->address->phone_num.'<br>';
        return $addressString;
    }

    public function getCarrierLink()
    {
        $prefix = null;
        if ($this->carrier === 'dhl' && $this->service === 'E-Commerce') {
            $prefix = 'http://webtrack.dhlglobalmail.com/?trackingnumber=';
        }

        if (is_null($prefix)) {
            return null;
        }

        return $prefix . ' ' . $this->tracking_num;
    }
}
