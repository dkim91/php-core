<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PriceList extends Eloquent
{
    protected $fillable = [
        'name',
    ];

    protected $appends = [];
    protected $hidden = [];

    public function customers()
    {
        return $this->hasMany('MerchPlatform\Entities\Customer');
    }

    public function items()
    {
        return $this->hasMany('MerchPlatform\Entities\PriceListItem');
    }
}
