<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class CampaignItem extends Eloquent
{
    protected $fillable = array('line_id', 'line_variant_id', 'campaign_order_id', 'variant_id', 'campaign_id', 'qty', 'cart_id', 'price_items', 'price_shipping', 'price_tax', 'upsell_discount_amount');
    protected $appends = array();
    protected $hidden = array();

    public function campaignItemExternals()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItemExternal');
    }

    public function packages()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Package', 'package_items', 'campaign_item_id', 'package_id');
    }

    public function packageItems()
    {
        return $this->hasMany('MerchPlatform\Entities\PackageItem');
    }

    public function supplierOrderItems()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrderItem');
    }

    public function cart()
    {
        return $this->belongsTo('MerchPlatform\Entities\Cart');
    }

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }

    public function line()
    {
        return $this->belongsTo('MerchPlatform\Entities\Line');
    }
    
    public function variant()
    {
        return $this->belongsTo('MerchPlatform\Entities\Variant');
    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    public function previewImages(){
        $pics = array();

        foreach($this->campaign->group->designs as $design)
        {
            foreach($design->designVariants as $designVariant)
            {
                if($designVariant->line->product_id == $this->variant->product_id && $designVariant->color_id == $this->variant->color_id)
                {
                    $pics[] = array('view' => $design->dec_position, 'fileName' => $designVariant->mockupFile->location);
                }
            }
        }
        return $pics;
    }

    public function previewFrontImage()
    {
        $imgs = $this->previewImages();
        if (isset($imgs) && count($imgs) > 0) {
            return $imgs[0]['fileName'];
        }
    }

    public function getPriceItemsAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceItemsAttribute($value)
    {
        $this->attributes['price_items'] = $value * 100;
    }

    public function getPriceShippingAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceShippingAttribute($value)
    {
        $this->attributes['price_shipping'] = $value * 100;
    }

    public function getPriceTaxAttribute($value)
    {
        return $value / 100;
    }


    public function setPriceTaxAttribute($value)
    {
        $this->attributes['price_tax'] = $value * 100;
    }

    public function getUpsellDiscountAmountAttribute($value)
    {
        return $value / 100;
    }

    public function setUpsellDiscountAmountAttribute($value)
    {
        $this->attributes['upsell_discount_amount'] = $value * 100;
    }


}
