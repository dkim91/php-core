<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class File extends Eloquent
{
    protected $softDelete = true;
    protected $fillable = [
        'original_id',
        'container',
        'location',
    ];

    public function original()
    {
        return $this->belongsTo('MerchPlatform\Entities\File', 'original_id');
    }

    public function converted()
    {
        return $this->hasOne('MerchPlatform\Entities\File', 'original_id');
    }

    public function designVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignVariant', 'mockup_file_id', 'id');
    }
    
}