<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Integration extends Eloquent
{
    protected $fillable = array(
        'customer_id',
        'integration_type',
        'integration_domain',
        'integration_access_key',
        'integration_access_token',
        'integration_access_secret'
    );
    protected $appends = array();
    protected $hidden = array();

    public $table = 'auth_integration';
    public $primaryKey = 'auth_integration_id';
    public $timestamps = true;
    
    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function collaborations()
    {
        return $this->hasMany('MerchPlatform\Entities\IntegrationCustomer', 'auth_integration_id');
    }

    public function campaignOrderExternals()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignOrderExternal', 'auth_integration_id');
    }

    public function campaignItemExternals()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItemExternal', 'auth_integration_id');
    }

}
