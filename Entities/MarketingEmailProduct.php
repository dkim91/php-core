<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmailProduct extends Eloquent
{
    protected $fillable = array('product_ids', 'customer_ids', 'subject', 'body', 'title');
    protected $appends = array();
    protected $hidden = array();

}
