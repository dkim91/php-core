<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Tag extends Eloquent
{
    protected $fillable = array('name');
    public $timestamps = false;

    public function campaigns()
    {
        return $this->belongsToMany('MerchPlatform\Entities\Campaign', 'campaign_tags');
    }

    public static function getFromNames($names)
    {
        $names = array_map(function ($item) {
            return trim($item['value']);
        }, $names);

        $tags = Tag::whereIn('name', $names)->get();

        $newTags = array_filter($names, function ($name) use ($tags) {
            foreach ($tags as $tag) {
                if ($tag->name === $name) {
                    return false;
                }
            }
            return true;
        });

        $newTags = array_filter($newTags);

        if (!empty($newTags)) {
            $newTags = array_map(function ($tag) {
                return \MerchPlatform\Entities\Tag::firstOrNew(['name' => $tag]);
            }, $newTags);
        }

        return array_merge($tags->all(), $newTags);
    }
}
