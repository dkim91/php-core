<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DBManager;

class Line extends Eloquent
{
    protected $fillable = array('product_id', 'campaign_price', 'campaign_base_cost', 'group_id', 'default_color_id', 'default_size_id', 'upsell_discount_amount');

    public function getPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function getCampaignPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setCampaignPriceAttribute($value)
    {
        $this->attributes['campaign_price'] = $value * 100;
    }

    public function getCampaignBaseCostAttribute($value)
    {
        return $value / 100;
    }

    public function setCampaignBaseCostAttribute($value)
    {
        $this->attributes['campaign_base_cost'] = $value * 100;
    }

    public function getUpsellDiscountAmountAttribute($value)
    {
        return money_format('%i', $value / 100);
    }

    public function setUpsellDiscountAmountAttribute($value)
    {
        $this->attributes['upsell_discount_amount'] = $value * 100;
    }


    // Line's parent relationships

    public function group()
    {
        return $this->belongsTo('MerchPlatform\Entities\Group');
    }

    public function product()
    {
        return $this->belongsTo('MerchPlatform\Entities\Product');
    }

    // Line's child relationships

    public function designs()
    {
        return $this->hasMany('MerchPlatform\Entities\Design');
    }

    public function lineVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\LineVariant');
    }

    public function campaignItems()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignItem');
    }

    public function designVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignVariant');
    }

    public function colors($withProductPhotos = false)
    {

        $colors = array(); $qtys = array(); $sizes = array(); $productPhotos = array();
        foreach ($this->lineVariants as $lineVariant) {
            $qtys[] = array('color_id' => $lineVariant->variant->color->id, 'qty' => $lineVariant->qty);
            $lineVariant->variant->color->cost = $lineVariant->cost;
            $colors[] = $lineVariant->variant->color;
            $sizes[] = array('id' => $lineVariant->variant->size_id, 'color_id' => $lineVariant->variant->color_id, 'qty' => $lineVariant->qty, 'name' => $lineVariant->variant->size->name);
        }

        $colors = array_unique($colors);
        foreach($colors as $color) {
            $sizesJson = array();
            $designVariantsJson = array();
            $productPhotosJson = array();

            foreach($sizes as $size) {

                if($color->id == $size['color_id']) {
                    $sizesJson[] = $size;
                    
                }
            }
            $color->sizes = json_encode($sizesJson);

            foreach($this->designVariants as $designVariant){
                if($designVariant->color_id == $color->id) {
                    $designVariantsJson[] = $designVariant;
                }
            }
            $color->designVariants = json_encode($designVariantsJson);

            if($withProductPhotos) {
                foreach($this->product->views as $view)
                {
                    foreach($view->viewFiles as $viewFile) {
                        if($viewFile->color_id == $color->id) {
                            $productPhotosJson[] = $viewFile;
                        }
                    }
                }
                $color->viewFiles = json_encode($productPhotosJson);
            }
        }
        
        

        $colors = \Illuminate\Database\Eloquent\Collection::make($colors);
        
        $colors = $colors->each(function($color) use($qtys, $sizes){
            foreach($qtys as $qty)
            {
                if($color->id == $qty['color_id'])
                {
                    $color->qty += $qty['qty'];
                }
            }

            $color->cost_per = money_format('%i', $color->cost/$color->qty);
        });
        
        return $colors;
    }

    public function deleteCascade()
    {
        \MerchPlatform\Entities\LineVariant::where('line_id', $this->id)->delete();
        $designVariants = \MerchPlatform\Entities\DesignVariant::where('line_id', $this->id)->get();
        $designVariants->each(function($designVariant){
            $designVariant->decColors()->detach();
            $designVariant->delete();
        });
        $this->delete();
    }
}
