<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SupplierOrderItem extends Eloquent
{
    protected $fillable = array('supplier_order_id', 'job_id', 'variant_id');
    protected $appends = array();
    protected $hidden = array();

    public $timestamps = false;

    public function supplierOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\SupplierOrder');
    }

    public function job()
    {
        return $this->belongsTo('MerchPlatform\Entities\Job');
    }

    public function variant()
    {
        return $this->belongsTo('MerchPlatform\Entities\Variant');
    }

    public function campaignItem()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignItem');
    }

    public function package()
    {
        return $this->belongsTo('MerchPlatform\Entities\Package');
    }
    
}