<?php namespace MerchPlatform\Entities;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Group extends Eloquent
{
    protected $fillable = [
        'campaign_id',
        'is_art_ready',
    ];

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }

    // Group's child relationships

    public function lines()
    {
        return $this->hasMany('MerchPlatform\Entities\Line');
    }

    public function lineVariants()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\LineVariant', 'MerchPlatform\Entities\Line');
    }

    public function designs()
    {
        return $this->hasMany('MerchPlatform\Entities\Design');
    }

    public function products()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\Product', 'MerchPlatform\Entities\Line');
    }

    public function deleteCascade()
    {
        // Delete all child lines and their children
        $this->lines->each(function($line){
            $line->deleteCascade();
        });
        // Delete all child designs and their children
        $this->designs->each(function($design){
            $design->deleteCascade();
        });

        // Delete this group
        $this->delete();
    }

    public function getDecMethodsAttribute()
    {
        $this->designs->each(function($design){
            $this->decMethods[] = strtoupper($design->dec_method);
        });
        $this->decMethods = array_unique($this->decMethods);

        return $this->decMethods;
    }

    public function getPrintLocationsAttribute()
    {
        $this->designs->each(function($design){
            $this->printLocations[] = $design->dec_position;
        });
        $this->printLocations = array_unique($this->printLocations);
        
        return $this->printLocations;
    }


}