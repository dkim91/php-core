<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class SupplierOrder extends Eloquent
{
    protected $fillable = array('warehouse_id', 'arrives_at', 'order_num', 'cost', 'status', 'response', 'contents', 'contractor_id', 'internal_order_num');
    protected $appends = array();
    protected $hidden = array();

    public function getDates()
    {
        return array('ordered_at', 'arrives_at', 'created_at', 'updated_at');
    }

    public function getCostAttribute($value)
    {
        return $value / 100;
    }

    public function setCostAttribute($value)
    {
        $this->attributes['cost'] = $value * 100;
    }

    public function warehouse()
    {
        return $this->belongsTo('MerchPlatform\Entities\Warehouse');
    }

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function payoutItem()
    {
        return $this->hasOne('MerchPlatform\Entities\PayoutItem');
    }

    public function lineVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\LineVariant');
    }

    public function items()
    {
        return $this->hasMany('MerchPlatform\Entities\SupplierOrderItem');
    }

}
