<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Brand extends Eloquent
{
    protected $fillable = [
        'name',
        'display_order',
        'code',
        'bodek_code',
        'ss_code',
        'sanmar_code',
        'broder_code',
        'aa_code',
        'slug',
        'description',
    ];
    protected $appends = array();
    protected $hidden = array();
    
    public function products()
    {
        return $this->hasMany('MerchPlatform\Entities\Product');
    }

    public function colors()
    {
        return $this->hasMany('MerchPlatform\Entities\Color');
    }

}