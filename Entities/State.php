<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class State extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();
    
    public function addresses()
    {
        return $this->hasMany('MerchPlatform\Entities\Address');
    }

}