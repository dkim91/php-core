<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Card extends Eloquent
{
    protected $fillable = [
        'customer_id',
        'stripe_token',
        'name',
        'type',
        'last_4',
    ];
    protected $appends = array();
    protected $hidden = array();

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }
}