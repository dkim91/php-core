<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Carbon\Carbon as Carbon;

class Upsell extends Eloquent
{
    protected $fillable = array(
        'customer_id',
        'is_active',
        'discount_flat'
    );
    protected $appends = array();
    protected $hidden = array();

    public $dates = array(
        'created_at',
        'updated_at'
    );

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function setDiscountFlatAttribute($value)
    {
        $this->attributes['discount_flat'] = $value * 100;
    }

    public function getDiscountFlatAttribute($cents)
    {
        return $cents / 100;
    }
}
