<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmail extends Eloquent
{
    protected $fillable = array('customer_id', 'marketing_email_filter_id', 'subject', 'body');

    public function marketingEmailFilter()
    {
        return $this->belongsTo('MerchPlatform\Entities\MarketingEmailFilter');
    }

    public function marketingEmailCampaigns()
    {
        return $this->hasMany('MerchPlatform\Entities\MarketingEmailCampaign');
    }

    public function marketingEmailHistories()
    {
        return $this->hasMany('MerchPlatform\Entities\MarketingEmailHistory');
    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

}