<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ScrRange extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();

    public function getAmountAttribute($value)
    {
        return $value / 100;
    }

    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = $value * 100;
    }

    public function getColors1Attribute($value)
    {
        return $value / 100;
    }

    public function setColors1Attribute($value)
    {
        $this->attributes['colors_1'] = $value * 100;
    }

    public function getColors2Attribute($value)
    {
        return $value / 100;
    }

    public function setColors2Attribute($value)
    {
        $this->attributes['colors_2'] = $value * 100;
    }

    public function getColors3Attribute($value)
    {
        return $value / 100;
    }

    public function setColors3Attribute($value)
    {
        $this->attributes['colors_3'] = $value * 100;
    }

    public function getColors4Attribute($value)
    {
        return $value / 100;
    }

    public function setColors4Attribute($value)
    {
        $this->attributes['colors_4'] = $value * 100;
    }

    public function getColors5Attribute($value)
    {
        return $value / 100;
    }

    public function setColors5Attribute($value)
    {
        $this->attributes['colors_5'] = $value * 100;
    }

    public function getColors6Attribute($value)
    {
        return $value / 100;
    }

    public function setColors6Attribute($value)
    {
        $this->attributes['colors_6'] = $value * 100;
    }

    public function getColors7Attribute($value)
    {
        return $value / 100;
    }

    public function setColors7Attribute($value)
    {
        $this->attributes['colors_7'] = $value * 100;
    }

    public function getColors8Attribute($value)
    {
        return $value / 100;
    }

    public function setColors8Attribute($value)
    {
        $this->attributes['colors_8'] = $value * 100;
    }

    public function getColors9Attribute($value)
    {
        return $value / 100;
    }

    public function setColors9Attribute($value)
    {
        $this->attributes['colors_9'] = $value * 100;
    }

    public function getColors10Attribute($value)
    {
        return $value / 100;
    }

    public function setColors10Attribute($value)
    {
        $this->attributes['colors_10'] = $value * 100;
    }

    public function getColors11Attribute($value)
    {
        return $value / 100;
    }

    public function setColors11Attribute($value)
    {
        $this->attributes['colors_11'] = $value * 100;
    }

    public function getColors12Attribute($value)
    {
        return $value / 100;
    }

    public function setColors12Attribute($value)
    {
        $this->attributes['colors_12'] = $value * 100;
    }

    public function getColors13Attribute($value)
    {
        return $value / 100;
    }

    public function setColors13Attribute($value)
    {
        $this->attributes['colors_13'] = $value * 100;
    }

    public function getColors14Attribute($value)
    {
        return $value / 100;
    }

    public function setColors14Attribute($value)
    {
        $this->attributes['colors_14'] = $value * 100;
    }

    public function getColors15Attribute($value)
    {
        return $value / 100;
    }

    public function setColors15Attribute($value)
    {
        $this->attributes['colors_15'] = $value * 100;
    }

    public function getShipTeesAttribute($value)
    {
        return $value / 100;
    }

    public function setShipTeesAttribute($value)
    {
        $this->attributes['ship_tees'] = $value * 100;
    }

    public function getShipSweatsAttribute($value)
    {
        return $value / 100;
    }

    public function setShipSweatsAttribute($value)
    {
        $this->attributes['ship_sweats'] = $value * 100;
    }

    public function priceTable()
    {
        return $this->belongsTo('MerchPlatform\Entities\PriceTable');
    }

}