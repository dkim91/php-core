<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Color extends Eloquent
{
    protected $fillable = [
        'brand_id',
        'name',
        'hex_prim',
        'hex_sec',
        'light_dark_prim',
        'light_dark_sec',
        'short_name',
    ];
    protected $appends = array();
    protected $hidden = array();
    
    public function brand()
    {
        return $this->belongsTo('MerchPlatform\Entities\Brand');
    }

    public function variants()
    {
        return $this->hasMany('MerchPlatform\Entities\Variant');
    }

    public function designVariants()
    {
        return $this->hasMany('MerchPlatform\Entities\DesignVariant');
    }

    public function viewFiles()
    {
        return $this->hasMany('MerchPlatform\Entities\ViewFile');
    }

}