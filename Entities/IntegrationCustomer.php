<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class IntegrationCustomer extends Eloquent
{
    protected $fillable = array('customer_id', 'auth_integration_id');
    protected $appends = array();
    protected $hidden = array();

    public $table = 'auth_integration_customers';
    public $primaryKey = 'auth_integration_customers_id';
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'customer_id');
    }

    public function integration()
    {
        return $this->belongsTo('MerchPlatform\Entities\Integration', 'auth_integration_id');
    }

}
