<?php namespace MerchPlatform\Entities;
use Illuminate\Database\Eloquent\Model as Eloquent;

class OrderReturn extends Eloquent
{
    protected $fillable = array();
    protected $appends = array();
    protected $hidden = array();

    protected $table = 'returns';

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function order()
    {
        return $this->belongsTo('MerchPlatform\Entities\Order');
    }

    public function refunds()
    {
        return $This->hasMany('MerchPlatform\Entities\Refund');
    }

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }

}