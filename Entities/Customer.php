<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DBManager;

class Customer extends Eloquent
{
    protected $fillable = [
        'admin_id',
        'parent_id',
        'login_at',
        'is_disabled',
        'is_registered',
        'is_affiliate',
        'first_name',
        'last_name',
        'email',
        'pass',
        'stripe_token',
        'auto_charge',
        'max_auto_charge',
        'company',
        'pwreset_key',
        'pwreset_count',
        'ip_signup',
        'ip_lastlogin',
        'ref_code',
        'tracker_key',
        'email_optout',
        'default_fb_pixel',
        'default_rt_pixel',
        'default_rt_google_pixel',
        'analytics_profile',
        'upsells_enabled',
        'bill_com_vendor_id',
        'bill_com_vendor_active',
        'fb_audience_pixel',
        'cross_sells_enabled',
    ];

    protected $appends = array('full_name');
    protected $hidden = array('pass');

    public function orderReturns()
    {
        return $this->hasMany('MerchPlatform\Entities\OrderReturn');
    }

    public function admin()
    {
        return $this->belongsTo('MerchPlatform\Entities\Admin');
    }

    public function parent()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'parent_id');
    }

    public function priceList()
    {
        return $this->belongsTo('MerchPlatform\Entities\PriceList');
    }

    public function referrals()
    {
        return $this->hasMany('MerchPlatform\Entities\Customer', 'parent_id');
    }

    public function discountCodes()
    {
        return $this->hasMany('MerchPlatform\Entities\DiscountCode', 'customer_id');
    }


    // Customer's child relationships
    public function integrations()
    {
        return $this->hasMany('MerchPlatform\Entities\Integration');
    }

    public function accessibleIntegrations()
    {
        return $this->hasMany('MerchPlatform\Entities\IntegrationCustomer', 'customer_id');
    }

    public function addresses()
    {
        return $this->hasMany('MerchPlatform\Entities\Address');
    }

    public function cards()
    {
        return $this->hasMany('MerchPlatform\Entities\Card');
    }

    public function cart()
    {
        return $this->hasOne('MerchPlatform\Entities\Cart');
    }

    public function groups()
    {
        return $this->hasMany('MerchPlatform\Entities\Group');
    }

    public function campaigns()
    {
        return $this->hasMany('MerchPlatform\Entities\Campaign');
    }

    public function orders()
    {
        // return $this->hasMany('MerchPlatform\Entities\Order');
        return $this->hasMany('MerchPlatform\Entities\Order', 'buyer_id');
    }

    public function payouts()
    {
        return $this->hasMany('MerchPlatform\Entities\Payout');
    }

    public function campaignOrders()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\CampaignOrder', 'MerchPlatform\Entities\Order');
    }

    public function paymentSources()
    {
        return $this->hasMany('MerchPlatform\Entities\PaymentSource');
    }

    public function getBalanceAttribute()
    {
        $campaigns = $this->campaigns()->endedSuccessfully()->notYetPaid()->with('order')->get();

        $this->calculatedBalance = 0.00;
        $campaigns->each(function($campaign){
            $this->calculatedBalance += $campaign->profit;
        });

        return money_format('%i', $this->calculatedBalance);
    }

    public function getIsRecipientAttribute()
    {
        if($this->attributes['stripe_rec_token']) {
            return true;
        }
        else {
            return false;
        }
    }

    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'].' '.$this->attributes['last_name'];
    }

    public function getMaxAutoChargeAttribute($value)
    {
        return $value / 100;
    }

    public function setMaxAutoChargeAttribute($value)
    {
        $this->attributes['max_auto_charge'] = $value * 100;
    }

    public function createIfNoEmail($email, $firstName, $lastName, $attributes = array())
    {
        $customer = \MerchPlatform\Entities\Customer::whereEmail($email)->get()->first();

        if(!$customer) {
            $billing = new \MerchPlatform\Billing\Stripe(
                \MerchPlatform\Config\Main::get('stripe')['secret'],
                \MerchPlatform\Config\Main::get('stripe')['apiDate']
            );
            $stripeCustomer = $billing->createCustomer(['email' => $email]);

            if (isset($stripeCustomer->id))
            {
                $data = array(
                    'email' => $email,
                    'first_name' => $firstName,
                    'last_name' => $lastName,
                    'stripe_token' => $stripeCustomer->id,
                    'tracker_key' => uniqid('track_', true)
                );
                $customer = \MerchPlatform\Entities\Customer::create(array_merge($data, $attributes));
            }
        }

        return $customer;

    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($user)
        {
            if ( !isset($user->tracker_key) || !$user->tracker_key){
                $user->attributes['tracker_key'] = uniqid('track_', true);
            }
        });
    }


}
