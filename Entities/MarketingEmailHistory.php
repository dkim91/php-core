<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MarketingEmailHistory extends Eloquent
{
    protected $fillable = array('marketing_email_id', 'customer_id', 'recipient_id', 'sent_at');


    public function marketingEmail()
    {
        return $this->belongsTo('MerchPlatform\Entities\MarketingEmail');

    }

    public function customer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer');
    }

    public function recipient()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'recipient_id');
    }

}