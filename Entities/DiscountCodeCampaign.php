<?php namespace MerchPlatform\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;

class DiscountCodeCampaign extends Eloquent
{
    protected $fillable = array(
        'discount_code_id',
        'campaign_id',
    );
    protected $appends = array();
    protected $hidden = array();
    public $dates = array('created_at', 'updated_at');

    public function discountCode()
    {
        return $this->belongsTo('MerchPlatform\Entities\DiscountCode');
    }

    public function campaign()
    {
        return $this->belongsTo('MerchPlatform\Entities\Campaign');
    }
}
