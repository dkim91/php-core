<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DBManager;
use Carbon\Carbon as Carbon;

class Order extends Eloquent
{
    protected $fillable = [
        'seller_id',
        'buyer_id',
        'address_id',
        'contractor_id',
        'ordered_at',
        'stripe_card_token',
        'stripe_charge_token',
        'purchased_via',
        'buyer_payment_status',
        'seller_payment_status',
        'seller_payout_status',
        'prod_status',
        'url_key',
        'buyer_cost',
        'seller_cost',
        'seller_payout',
        'notes_internal',
        'notes',
        'status',
        'external_id',
        'external_customer_order_id',
        'external_customer_order_num',
        'external_vendor_order_id',
        'external_vendor_order_num',
    ];
    protected $appends = array();
    protected $hidden = array();

    public function getBuyerCostAttribute($value)
    {
        return $value / 100;
    }

    public function setBuyerCostAttribute($value)
    {
        $this->attributes['buyer_cost'] = $value * 100;
    }

    public function getSellerCostAttribute($value)
    {
        return $value / 100;
    }

    public function setSellerCostAttribute($value)
    {
        $this->attributes['seller_cost'] = $value * 100;
    }

    public function getSellerPayoutAttribute($value)
    {
        return $value / 100;
    }

    public function setSellerPayoutAttribute($value)
    {
        $this->attributes['seller_payout'] = $value * 100;
    }

    public function getPriceCampaignAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceCampaignAttribute($value)
    {
        $this->attributes['price_campaign'] = $value * 100;
    }

    public function getPriceBulkAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceBulkAttribute($value)
    {
        $this->attributes['price_bulk'] = $value * 100;
    }

    public function getPriceExternalAttribute($value)
    {
        return $value / 100;
    }

    public function setPriceExternalAttribute($value)
    {
        $this->attributes['price_external'] = $value * 100;
    }

    public function getPayoutCustomerAttribute($value)
    {
        return $value / 100;
    }

    public function getPaymentStatusAttribute()
    {
        if ($this->seller_cost > 0) {
            return $this->seller_payment_status;
        }
        return $this->buyer_payment_status;
    }

    public function setPayoutCustomerAttribute($value)
    {
        $this->attributes['payout_customer'] = $value * 100;
    }

    public function getPayoutSupplierAttribute($value)
    {
        return $value / 100;
    }

    public function setPayoutSupplierAttribute($value)
    {
        $this->attributes['payout_supplier'] = $value * 100;
    }

    public function getPayoutContractorAttribute($value)
    {
        return $value / 100;
    }

    public function setPayoutContractorAttribute($value)
    {
        $this->attributes['payout_contractor'] = $value * 100;
    }

    public function getPayoutPaygatewayAttribute($value)
    {
        return $value / 100;
    }

    public function setPayoutPaygatewayAttribute($value)
    {
        $this->attributes['payout_paygateway'] = $value * 100;
    }

    public function getCostExtraSizesAttribute($value)
    {
        return $value / 100;
    }

    public function setCostExtraSizesAttribute($value)
    {
        $this->attributes['cost_extra_sizes'] = $value * 100;
    }

    public function contractor()
    {
        return $this->belongsTo('MerchPlatform\Entities\Contractor');
    }

    public function buyer()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'buyer_id');
    }

    public function seller()
    {
        return $this->belongsTo('MerchPlatform\Entities\Customer', 'seller_id');
    }

    public function address()
    {
        return $this->belongsTo('MerchPlatform\Entities\Address');
    }

    // Order's child relationships

    public function orderReturn()
    {
        return $this->hasOne('MerchPlatform\Entities\OrderReturn');
    }

    public function groups()
    {
        return $this->hasMany('MerchPlatform\Entities\Group');
    }

    public function campaign()
    {
        return $this->hasOne('MerchPlatform\Entities\Campaign');
    }

    public function campaignOrders()
    {
        return $this->hasMany('MerchPlatform\Entities\CampaignOrder');
    }

    public function campaignOrderProblem()
    {
        return $this->hasOne('MerchPlatform\Entities\CampaignOrderProblem');
    }

    public function isReprint()
    {
        return $this->hasOne('MerchPlatform\Entities\CampaignOrderProblem', 'id', 'reprint_order_id');
    }

    public function packages()
    {
        return $this->hasMany('MerchPlatform\Entities\Package');
    }

    public function refunds()
    {
        return $this->hasMany('MerchPlatform\Entities\Refund');
    }

    public function payoutItem()
    {
        return $this->hasOne('MerchPlatform\Entities\PayoutItem');
    }

    public function jobs()
    {
        return $this->hasMany('MerchPlatform\Entities\Job');
    }

    public function getOrderNumAttribute()
    {
        return str_pad($this->id, 4, 0, STR_PAD_LEFT);
    }

    public function campaignPackages()
    {
        return $this->hasManyThrough('MerchPlatform\Entities\Package', 'MerchPlatform\Entities\CampaignOrder');
    }

    public function scopeIsOrderNow($query)
    {
        return $query->where('price_bulk', '>', 0)->has('campaign', '=', 0);
    }

    public function scopeStatusIs($query, $status)
    {
        return $query->whereProdStatus($status);
    }

    public function scopeCampaign($query)
    {
        return $query->where('price_bulk', '>', 0)->has('campaign', '=', 1);
    }

    public function getDates()
    {
        return array('shippingdue_at', 'created_at', 'updated_at', 'paid_at');
    }

    public function getTotalPiecesAttribute()
    {
        $this->totalPiecesAttr = 0;
        $this->groups->each(function($group){
            $group->lines->each(function($line){
                $line->lineVariants->each(function($lineVariant){
                    $this->totalPiecesAttr += $lineVariant->qty;
                });
            });
        });
        
        return $this->totalPiecesAttr;
    }

    // Sets custom "prod_status_label" attribute (bootstrap label color class name).. Based on the Order's 'prod_status' value
    public function getProdStatusLabelAttribute()
    {
        if($this->attributes['prod_status'] == 'In Review')
        {
            return 'default';
        }
        elseif($this->attributes['prod_status'] == 'In Production')
        {
            return 'warning';
        }
        elseif($this->attributes['prod_status'] == 'Shipped')
        {
            return 'info';
        }
        elseif($this->attributes['prod_status'] == 'Delivered')
        {
            return 'success';
        }

        return 'default';
    }

    // Custom attribute. Array with two values. 'hours_left' is the number of hours since the Order was created. 'label' is the bootstrap label to use (color)
    public function getApprovalDeadlineAttribute()
    {
        $hoursSinceCreated = $this->created_at->diffInHours(Carbon::now(), false);
        $hoursLeft = 36-$hoursSinceCreated;

        if($hoursLeft >= 24)
        {
            $label = 'success';
        }
        elseif($hoursLeft >= 12)
        {
            $label = 'warning';
        }
        else
        {
            $label = 'danger';
        }

        $approvalDeadline = array('hours_left' => $hoursLeft, 'label' => $label);

        return $approvalDeadline;
    }

    public function getRawPaidExternalOrderData()
    {
        $orderData = DBManager::select(
            'SELECT
                auth_integration.integration_domain as domain,
                campaign_order_externals.campaign_order_id,
                customers.email,
                campaign_orders.url_key,
                campaign_items.qty,
                campaign_items.created_at,
                concat(products.short_name, \', \', colors.name, \', \', sizes.name) as item_name,
                sum(campaign_items.price_items + campaign_items.price_shipping) as revenue,
                sum(campaign_items.qty * lines.campaign_base_cost) as base_due,
                sum(campaign_items.qty * 100 * (CASE WHEN countries.is_domestic > 0 THEN rates.ship_dom ELSE rates.ship_intl END)) as shipping_due,
                sum((campaign_items.qty * lines.campaign_base_cost) + (campaign_items.qty * 100 * (CASE WHEN countries.is_domestic > 0 THEN rates.ship_dom ELSE rates.ship_intl END))) as total_due
            from (select
                    CONVERT((select value from settings where `key` = \'external_domestic_shipping_cost_usd\'), DECIMAL(5, 2)) as ship_dom,
                    CONVERT((select value from settings where settings.`key` = \'external_international_shipping_cost_usd\'), DECIMAL(5, 2)) as ship_intl
                ) as rates,
                campaign_order_externals
            inner join auth_integration
                on campaign_order_externals.auth_integration_id = auth_integration.auth_integration_id
            inner join campaign_orders
                on campaign_order_externals.campaign_order_id = campaign_orders.id
            inner join campaigns
                on campaign_orders.campaign_id = campaigns.id
            inner join campaign_items
                on campaign_orders.id = campaign_items.campaign_order_id
            inner join `lines`
                on campaign_items.line_id = lines.id
            inner join variants
                on campaign_items.variant_id = variants.id
            inner join products
                on variants.product_id = products.id
            inner join colors
                on variants.color_id = colors.id
            inner join sizes
                on variants.size_id = sizes.id
            inner join orders
                on campaign_orders.order_id = orders.id
            inner join customers
                on orders.customer_id = customers.id
            inner join addresses
                on orders.address_id = addresses.id
            inner join countries
                on addresses.country_id = countries.id
            where campaigns.order_id = ' . $this->id . '
                and campaign_orders.status = \'paid\'
            group by campaign_items.id
            order by campaign_items.created_at'
        );
        return $orderData;
    }

    public function getTotalSavings()
    {
        $totalSavings = 0.00;
        foreach ($this->campaignOrders as $mo) {
            $totalSavings += $mo->price_savings;
        }

        return $totalSavings;
    }

    public function getTotalItemsPrice()
    {
        $totalItemsPrice = 0.00;
        foreach ($this->campaignOrders as $mo) {
            $totalItemsPrice += $mo->price_items;
            $totalItemsPrice += $mo->price_savings;
        }

        return $totalItemsPrice;
    }

    public function getTotalTaxesPrice()
    {
        $totalTaxesPrice = 0.00;
        foreach ($this->campaignOrders as $mo) {
            $totalTaxesPrice += $mo->price_tax;
        }

        return $totalTaxesPrice;
    }

    public function getTotalShippingPrice()
    {
        $totalShippingPrice = 0.00;
        foreach ($this->campaignOrders as $mo) {
            $totalShippingPrice = $mo->price_shipping;
        }

        return $totalShippingPrice;
    }

    public function getSubtotal()
    {
        return $this->getTotalItemsPrice() + $this->getTotalShippingPrice() + $this->getTotalTaxesPrice() + $this->getTotalSavings();
    }
}
