<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class DesignVariant extends Eloquent
{
    protected $fillable = array('design_id', 'line_id', 'color_id', 'printer_file_id', 'mockup_file_id', 'design_file_id', 'dec_method', 'num_colors');
    protected $appends = array();
    protected $hidden = array();
    
    public function design()
    {
        return $this->belongsTo('MerchPlatform\Entities\Design');
    }

    public function line()
    {
        return $this->belongsTo('MerchPlatform\Entities\Line');
    }

    public function color()
    {
        return $this->belongsTo('MerchPlatform\Entities\Color');
    }

    public function mockupFile()
    {
        return $this->belongsTo('MerchPlatform\Entities\File', 'mockup_file_id', 'id');
    }

    public function printerFile()
    {
        return $this->belongsTo('MerchPlatform\Entities\File', 'printer_file_id', 'id');
    }

    public function dtgFile()
    {
        return $this->belongsTo('MerchPlatform\Entities\File', 'dtg_file_id', 'id');
    }

    public function designFile()
    {
        return $this->belongsTo('MerchPlatform\Entities\File', 'design_file_id', 'id');
    }

    public function decColors()
    {
        return $this->belongsToMany('MerchPlatform\Entities\DecColor', 'dec_color_design', 'design_variant_id', 'dec_color_id');
    }

}