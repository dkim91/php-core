<?php namespace MerchPlatform\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;

class DiscountCodeCampaignOrder extends Eloquent
{
    protected $fillable = array(
        'discount_code_id',
        'campaign_order_id',
    );
    protected $appends = array();
    protected $hidden = array();
    public $dates = array('created_at', 'updated_at');

    public function discountCode()
    {
        return $this->belongsTo('MerchPlatform\Entities\DiscountCode');
    }

    public function campaignOrder()
    {
        return $this->belongsTo('MerchPlatform\Entities\CampaignOrder');
    }
}
