<?php namespace MerchPlatform\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PriceListItem extends Eloquent
{
    protected $fillable = [
        'variant_id',
        'price_list_id',
        'prod_price',
        'prod_add_price',
        'ship_usa_price',
        'ship_can_price',
        'ship_int_price',
        'ship_usa_add_price',
        'ship_can_add_price',
        'ship_int_add_price',
    ];

    protected $appends = [];
    protected $hidden = [];

    public function priceList()
    {
        return $this->belongsTo('MerchPlatform\Entities\PriceList');
    }

    public function variant()
    {
        return $this->belongsTo('MerchPlatform\Entities\Variant');
    }

    public function setProdPriceAttribute($value)
    {
        $this->attributes['prod_price'] = $value * 100;
    }

    public function getProdPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setProdAddPriceAttribute($value)
    {
        $this->attributes['prod_add_price'] = $value * 100;
    }

    public function getProdAddPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipUsaPriceAttribute($value)
    {
        $this->attributes['ship_usa_price'] = $value * 100;
    }

    public function getShipUsaPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipCanPriceAttribute($value)
    {
        $this->attributes['ship_can_price'] = $value * 100;
    }

    public function getShipCanPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipIntPriceAttribute($value)
    {
        $this->attributes['ship_int_price'] = $value * 100;
    }

    public function getShipIntPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipUsaAddPriceAttribute($value)
    {
        $this->attributes['ship_usa_add_price'] = $value * 100;
    }

    public function getShipUsaAddPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipCanAddPriceAttribute($value)
    {
        $this->attributes['ship_can_add_price'] = $value * 100;
    }

    public function getShipCanAddPriceAttribute($value)
    {
        return $value / 100;
    }

    public function setShipIntAddPriceAttribute($value)
    {
        $this->attributes['ship_int_add_price'] = $value * 100;
    }

    public function getShipIntAddPriceAttribute($value)
    {
        return $value / 100;
    }
}
