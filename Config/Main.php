<?php namespace MerchPlatform\Config;

use Illuminate\Database\Capsule\Manager as DBManager;

class Main
{
    public static function load($fileLocation = '/etc/config/config.json')
    {
        $config = json_decode(file_get_contents($fileLocation), true);
        $GLOBALS['mpConfig'] = $config;
    }

    public static function allPublic()
    {
        $publicProperties = [
            'brand',
            'address',
        ];

        $publicArray = array_only($GLOBALS['mpConfig'], $publicProperties);

        return $publicArray;
    }

    public static function get($key)
    {
        if (!isset($GLOBALS['mpConfig'])) {
            self::load();
        }

        return $GLOBALS['mpConfig'][$key];
    }

    public static function loadDb()
    {
        $config = [
            'driver' => 'mysql',
            'host' => self::get('databases')['mysql']['connection']['host'],
            'database' => self::get('databases')['mysql']['connection']['database'],
            'username' => self::get('databases')['mysql']['connection']['user'],
            'password' => self::get('databases')['mysql']['connection']['password'],
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ];

        $DBManager = new DBManager;
        $DBManager->addConnection($config);
        $DBManager->setAsGlobal();
        $DBManager->bootEloquent();
    }
}
