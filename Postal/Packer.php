<?php
namespace MerchPlatform\Postal;

use MerchPlatform\Entities\PackageType;
use MerchPlatform\Entities\Package;
use Illuminate\Database\Eloquent\Collection;
use stdClass;

/**
 * Intelligently pack supplier order items into packages
 */
class Packer
{
    protected $packageTypes;
    protected $items;
    protected $packages;
    protected $addressId;

    const MAX_CHARS_CUSTOM_LINE = 45;

    /**
     * Construct the packer class by passing in package types and items
     * @param object $packageTypes Eloquent collection of PackageType models
     * @param object $items Eloquent collection of SupplierOrderItem models
     */
    public function __construct($packageTypes, $items, $addressId = null)
    {
        $this->packageTypes = $packageTypes;
        $this->items = $items;
        $this->packages = new Collection();
        $this->addressId = $addressId;
    }

    /**
     * Pack the items in packages the most efficient way
     * @return object Eloquent collection of the packed packages
     */
    public function packItems()
    {
        $packageType = $this->getBestPackageType();
        $numPackagesNeeded = $this->getNumPackagesNeeded($packageType);
        $this->createPackages($packageType, $numPackagesNeeded);
        $this->fillPackages();
        $this->addLabelLines();
        return $this->packages;
    }

    /**
     * Chooses the package type to use based on the least amount of packages required
     * If there is a tie, then it chooses the smaller of the package types
     * @param  collection $items Collection of item objects
     * @return object        the chosen packageType object
     */
    public function getBestPackageType()
    {
        $chosenPackageType = $chosenPackagesNeeded = null;
        foreach ($this->packageTypes as $packageType) {
            $packagesNeeded = $this->getNumPackagesNeeded($packageType);
            if (is_null($chosenPackageType)) {
                $chosenPackageType = $packageType;
                $chosenPackagesNeeded = $packagesNeeded;
            } elseif ($packagesNeeded < $chosenPackagesNeeded) {
                $chosenPackageType = $packageType;
                $chosenPackagesNeeded = $packagesNeeded;
            } elseif ($packagesNeeded === $chosenPackagesNeeded) {
                if ($packageType->weight_oz < $chosenPackageType->weight_oz) {
                    $chosenPackageType = $packageType;
                    $chosenPackagesNeeded = $packagesNeeded;
                }
            }
        }
        return $chosenPackageType;
    }

    /**
     * Calculate the number of packages needed if you were to pack all items
     * in the specified PackageType
     * @param  object $packageType Eloquent PackageType model
     * @return int The number of packages needed
     */
    private function getNumPackagesNeeded($packageType)
    {
        $totalSlotsNeeded = 0;
        $slotsPerPackage = $this->getSlotsPerPackage($packageType);
        foreach ($this->items as $item) {
            $slotsTakenPer = $this->getSlotsTakenPer($item, $packageType);
            $totalSlotsNeeded += $slotsTakenPer;
        }
        $packagesNeeded = ($totalSlotsNeeded / $slotsPerPackage);
        return ceil($packagesNeeded);
    }

    /**
     * Create as many packages needed to pack all items
     * @param  object $packageType Eloquent PackageType model
     * @param  int $numPackagesNeeded Number of packages to create
     * @return object Eloquent collection of package models
     */
    public function createPackages($packageType, $numPackagesNeeded)
    {
        for ($i = 0; $i < $numPackagesNeeded; $i++) {
            $package = new Package();
            $package->package_type_id = $packageType->id;
            $package->length = $packageType->length;
            $package->width = $packageType->width;
            $package->height = $packageType->height;
            $package->weight_oz = $packageType->weight_oz;
            if ($this->addressId) {
                $package->address_id = $this->addressId;
            }
            $slotsTotal = $this->getSlotsPerPackage($packageType);
            $package->metaData = new stdClass();
            $package->metaData->slotsTaken = 0;
            $package->metaData->slotsTotal = $slotsTotal;
            $package->metaData->packageType = $packageType;
            $package->metaData->items = array();
            $this->packages->add($package);
        }
        return $this->packages;
    }

    /**
     * Fill the packages with the items
     * @return object Eloquent collection of package models
     */
    public function fillPackages()
    {
        foreach ($this->packages as $package) {
            foreach ($this->items as $item) {
                if ($this->isItemPacked($item)) {
                    continue;
                }
                if ($this->doesPackageFitItem($package, $item)) {
                    $this->packItem($package, $item);
                }
            }
        }
        return $this->packages;
    }

    /**
     * Determine and set the label lines for each package
     * @return object Eloquent collection of package models
     */
    public function addLabelLines()
    {
        foreach ($this->packages as $package) {
            $itemsWithQty = $entries = array();
            $labelLines = ['', '', ''];
            if ($package->metaData->items[0]->campaignItem) {
                $entries[] = $package->metaData->items[0]
                    ->campaignItem->campaignOrder->order_num;
            }
            foreach ($package->metaData->items as $item) {
                if (!isset($itemsWithQty[$item->variant_id])) {
                    $itemsWithQty[$item->variant_id]['qty'] = 0;
                    $itemsWithQty[$item->variant_id]['variant'] = $item->variant;
                }
                $itemsWithQty[$item->variant_id]['qty'] += 1;
            }
            foreach ($itemsWithQty as $itemWithQty) {
                $entries[] = implode(' ', array(
                    $itemWithQty['qty'],
                    'x',
                    $itemWithQty['variant']->size->name,
                    $itemWithQty['variant']->product->code,
                    $itemWithQty['variant']->color->short_name
                ));
            }
            foreach ($entries as $entry) {
                for($i = 0; $i < 3; $i++) {
                    $currentLength = strlen($labelLines[$i]);
                    if ($currentLength + strlen($entry) > self::MAX_CHARS_CUSTOM_LINE) {
                        continue;
                    }
                    if ($currentLength) {
                        $labelLines[$i] .= ', ';
                    }
                    $labelLines[$i] .= $entry;
                    break;
                }
            }
            $package->label_line_1 = $labelLines[0];
            $package->label_line_2 = $labelLines[1];
            $package->label_line_3 = $labelLines[2];
        }
        return $this->packages;
    }

    /**
     * Pack an item in a package
     * @param  object $package Eloquent Package model to pack item in
     * @param  object $item    Eloquent SupplierOrderItem model to pack
     * @return object Eloquent Package model
     */
    public function packItem($package, $item)
    {
        $slotsPer = $this->getSlotsTakenPer(
            $item,
            $package->metaData->packageType
        );
        $package->weight_oz += $item->variant->weight_oz;
        $package->metaData->items[] = $item;
        $package->metaData->slotsTaken += $slotsPer;
        return $package;
    }

    /**
     * Save the packed packages and their items' relations to database
     * @return bool Returns true if successful, else false
     */
    public function saveToDatabase()
    {
        $result = true;
        foreach ($this->packages as $package)
        {
            $items = $package->metaData->items;
            if (isset($items[0]->campaignItem->campaign_order_id)) {
                $package->campaign_order_id = $items[0]->campaignItem->campaign_order_id;
            }
            unset($package->metaData);
            if (!$package->save()) {
                $result = false;
            }
            if (!$package->supplierOrderItems()->saveMany($items)) {
                $result = false;
            }
        }
        return $result;
    }

    /**
     * Get the total number of slots a specified PackageType has
     * @param  object $packageType Eloquent PackageType model
     * @return int The total number of slots
     */
    private function getSlotsPerPackage($packageType)
    {
        $fitsValues = array();
        foreach ($this->items as $item) {
            $product = $item->variant->product;
            $fitsValue = $product->{'fits_'.$packageType->short_name};
            $fitsValues[$fitsValue] = $fitsValue;
        }
        $slotsPerPackage = $this->getLcm(array_keys($fitsValues));
        return $slotsPerPackage;
    }

    /**
     * Get the number of slots a specified item will occupy in a specified
     * PackageType
     * @param  object $item            Eloquent SupplierOrderItem model
     * @param  object $packageType     Eloquent PackageType model
     * @param  int $slotsPerPackage 
     * @return [type]                  [description]
     */
    private function getSlotsTakenPer($item, $packageType)
    {
        $slotsPerPackage = $this->getSlotsPerPackage($packageType);
        $product = $item->variant->product;
        $fitsValue = $product->{'fits_'.$packageType->short_name};
        $slotsTakenPer = ($slotsPerPackage / $fitsValue);
        return $slotsTakenPer;
    }

    /**
     * Check if an item is already packed in a package
     * @param  object  $item Eloquent SupplierOrderItem model
     * @return boolean Returns true if yes, else false
     */
    private function isItemPacked($item)
    {
        foreach ($this->packages as $package) {
            foreach ($package->metaData->items as $packedItem) {
                if ($packedItem->id === $item->id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Check if a specified package can fit a specified item
     * @param  object $package Eloquent Package model
     * @param  object $item    Eloquent SupplierOrderItem model
     * @return bool true if it can fit, else false
     */
    private function doesPackageFitItem($package, $item)
    {
        $slotsPer = $this->getSlotsTakenPer(
            $item,
            $package->metaData->packageType
        );
        $slotsLeft = ($package->metaData->slotsTotal - $package->metaData->slotsTaken);
        if ($slotsPer <= $slotsLeft) {
            return true;
        }
        return false;
    }
    
    /**
     * Get the least common multiple of a group of numbers
     * @param  array $values The values to get the least common multiple of
     * @return  int The least common multiple
     */
    private function getLcm($values)
    {
        while (2 <= count($values)) {
            array_push(
                $values,
                $this->lcm(
                    array_shift($values),
                    array_shift($values)
                )
            );
        }
        return reset($values);
    }

    /**
     * Get the least common multiple of two numbers
     * @param  int $n The first number
     * @param  int $m The second number
     * @return  int The least common multiple of the two numbers
     */
    private function lcm($n, $m)
    {
        $lcm = ($m * ($n / $this->gcd($n, $m)));
        return $lcm;
    }

    /**
     * Get the greatest common denominator of two numbers 
     * @param  int $n The first number
     * @param  int $m The second number
     * @return  int The greatest common denominator of the two numbers
     */
    private function gcd($n, $m)
    {
        $n = abs($n);
        $m = abs($m);
        if (($n == 0) && ($m == 0)) {
            return 1;
        }
        if (($n == $m) && ($n >= 1)) {
            return $n;
        }
        $gcd = ($m < $n ? $this->gcd(($n - $m), $n) : $this->gcd($n, ($m-$n)));
        return $gcd;
    }
}
